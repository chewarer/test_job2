import json
import asyncio
import aiohttp
from aiohttp.client_exceptions import ClientError
from operator import itemgetter
from django.http import HttpResponse
from django.views.generic import View
from django.contrib.sites.shortcuts import get_current_site


class AccessPoint(View):
    """
        Точка доступа к разным источникам данных.
        Делает асинхронный запрос к источникам данных указанных в URLS.
        Если источник данных вернул ошибку или нет ответа более 2 сек, игнорирует его,
        и возвращает результат с теми данными что получены от прочих ресурсов.
    """
    def __init__(self, **kwargs):
        super(AccessPoint, self).__init__(**kwargs)
        self.result = []
        self.URLS = []

    async def get_data_from(self, url):
        """Запрашивает данные асинхронно со всех URL"""
        try:
            response = await aiohttp.request(method='GET', url=url)
        except ClientError:
            return

        if response.status in (200, ):
            json_resp = await response.json()
            # Добавление полученных данных в результирующий список
            for x in json_resp:
                if x not in self.result:
                    self.result.append(x)
        response.close()

    async def set_tasks(self):
        """Создание списка задач"""
        tasks = [asyncio.ensure_future(
            self.get_data_from(url)) for url in self.URLS]
        await asyncio.wait(tasks, timeout=2)

    def get(self, request):
        """Принимает GET запрос и отвечает на него"""
        host = 'http://{}'.format(str(get_current_site(request)))
        self.URLS = [
            host + '/sources/api/1',
            host + '/sources/api/2',
            # host + '/sources/api/3',
            'http://78.155.199.19:8000/api/3/',  # данные на внешнем сервере
            'http://some_invalid_url.com/100500',
        ]

        ioloop = asyncio.new_event_loop()
        asyncio.set_event_loop(ioloop)
        ioloop.run_until_complete(self.set_tasks())
        ioloop.close()
        self.result.sort(key=itemgetter('id'))

        return HttpResponse(json.dumps(self.result, ensure_ascii=False), content_type='application/json', status=200)
