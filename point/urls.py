from django.conf.urls import url
from point.views import AccessPoint

urlpatterns = [
	url(r'^api/$', AccessPoint.as_view(), name='point'),
]
