import json
import urllib
from django.http import HttpResponse, JsonResponse
from django.views.generic import View
from django.contrib.sites.shortcuts import get_current_site


class Main(View):
    """Главный клиент, с которого идет запрос к точке доступа, запрашивающей данные из разных источников"""
    URL = '/point/api/'

    def get(self, request):
        host = str(get_current_site(request))
        url = 'http://{}{}'.format(str(host), self.URL)
        print(url)
        response = urllib.request.urlopen(url)
        if response.status in (200, ):
            data = response.read().decode('utf-8')
            data = json.loads(data)
            return HttpResponse(json.dumps(data, ensure_ascii=False), content_type='application/json', status=200)

        return JsonResponse({}, status=400)
