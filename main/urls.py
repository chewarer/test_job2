from django.conf.urls import url
from main.views import Main

urlpatterns = [
    url(r'^api/$', Main.as_view(), name='main'),
]
