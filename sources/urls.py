from django.conf.urls import url
from sources.views import SourceOne, SourceTwo

urlpatterns = [
	url(r'^api/1/$', SourceOne.as_view(), name='source_1'),
	url(r'^api/2/$', SourceTwo.as_view(), name='source_2'),
	# url(r'^api/3/$', SourceThree.as_view(), name='source_3'),
]
