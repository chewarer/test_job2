import json
from django.http import HttpResponse
from django.views.generic import View


class Source(View):
    """
        Базовый класс для разных источников данных.
        Делаю ресурсы разными классами и с разными URL, т.к. они по определению разные источники.
    """
    def __init__(self, **kwargs):
        super(Source, self).__init__(**kwargs)
        self.values = []
        self.val_range = None  # ((1, 10), (31, 40))

    def gen_values(self, nums):
        """Генерирует словарь с ключами в выбранном диапазоне"""
        for i in range(nums[0], nums[1] + 1):
            self.values.append(dict([
                ('id', i),
                ('name', 'Name %s' % i)
            ]))

    def get(self, request):
        """Отдает список данных в формате Json"""
        # Генерируем ключи для каждого диапазона
        for nums in self.val_range:
            self.gen_values(nums)

        return HttpResponse(json.dumps(self.values, ensure_ascii=False), content_type='application/json', status=200)


class SourceOne(Source):
    """Первый источник данных"""
    def __init__(self, **kwargs):
        super(SourceOne, self).__init__(**kwargs)
        self.val_range = ((1, 10), (31, 40))


class SourceTwo(Source):
    """Второй источник данных"""
    def __init__(self, **kwargs):
        super(SourceTwo, self).__init__(**kwargs)
        self.val_range = ((1, 20), (41, 50))


# class SourceThree(Source):
#     """Третий источник данных"""
#     def __init__(self, **kwargs):
#         super(SourceThree, self).__init__(**kwargs)
#         self.val_range = ((1, 30), (51, 60))
